#include "effect.h"

using namespace Wavr;

Effect::Effect(AudioContext &context) : m_context(context) {}
bool Effect::process(AudioContext &, data_t &) { return true; }
AudioContext &Effect::get_context() { return m_context; }
