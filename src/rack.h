#include "context.h"
#include "effect.h"
#include <vector>

namespace Wavr {
typedef std::vector<Effect *> effect_list_t;
class Rack {
public:
    typedef std::vector<std::vector<float>> data_t;
    Rack(AudioContext &ctx);
    bool insert_effect(Effect *effect);
    bool insert_effect(Effect *effect, unsigned short pos);
    bool process(data_t &data);
    AudioContext &get_context();

private:
    AudioContext m_ctx;
    effect_list_t m_nodes;
};
}; // namespace Wavr
