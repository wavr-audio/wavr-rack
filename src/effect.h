#pragma once
#include "context.h"
#include <vector>

namespace Wavr {
class Effect {
public:
    typedef std::vector<std::vector<float>> data_t;
    Effect(AudioContext &context);
    virtual ~Effect() = default;
    virtual bool process(AudioContext &, data_t &data);

protected:
    AudioContext &get_context();

private:
    AudioContext m_context;
};
}; // namespace Wavr
