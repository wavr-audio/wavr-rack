#include "rack.h"
#include <iostream>
using namespace Wavr;

Rack::Rack(AudioContext &ctx) : m_ctx(ctx) {}
bool Rack::insert_effect(Effect *effect) {
    m_nodes.push_back(effect);
    return true;
}
bool Rack::insert_effect(Effect *effect, unsigned short pos) {
    if (pos > m_nodes.size()) {
        m_nodes.push_back(effect);
        return true;
    }
    auto position = m_nodes.begin() + pos;
    m_nodes.insert(position, effect);
    return true;
}

bool Rack::process(data_t &data) {
    auto ctx = get_context();
    for (unsigned int i = 0; i < m_nodes.size(); i++) {
        m_nodes.at(i)->process(ctx, data);
    }
    return true;
}

AudioContext &Rack::get_context() { return m_ctx; }
