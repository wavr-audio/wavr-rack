#pragma once

namespace Wavr {
enum CONTEXT_STATE { STATE_PAUSED, STATE_PLAYING, STATE_OFFLINE, STATE__COUNT };
class AudioContext {
public:
    AudioContext(unsigned long sample_rate, unsigned short channelcount);
    unsigned long get_samplerate();
    unsigned short get_channelcount();
    CONTEXT_STATE get_state();
    bool set_state(CONTEXT_STATE state);

private:
    unsigned long m_samplerate;
    unsigned short m_channelcount;
    CONTEXT_STATE m_state;
};
}; // namespace Wavr
