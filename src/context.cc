#include "context.h"

using namespace Wavr;

AudioContext::AudioContext(unsigned long sample_rate, unsigned short channelcount)
    : m_samplerate(sample_rate), m_channelcount(channelcount), m_state(STATE_PAUSED) {}
unsigned long AudioContext::get_samplerate() { return m_samplerate; }
unsigned short AudioContext::get_channelcount() { return m_channelcount; }

CONTEXT_STATE AudioContext::get_state() { return m_state; }

bool AudioContext::set_state(CONTEXT_STATE state) { return true; }
