#include "rack.h"

#include "test_effects.h"

int main() {
    size_t samplerate = 44100L;
    unsigned short channels = 2;
    size_t total_samples = samplerate * 5;
    unsigned int frame_size = 256;
    Wavr::AudioContext ctx(samplerate, channels);
    Wavr::Rack rack(ctx);
    Wavr::Test::SineWaveEffect eff_sine(ctx, 80, 1.0);
    Wavr::Test::DistortionEffect eff_dist(ctx, 2.5);
    Wavr::Test::PCMWriterEffect eff_write(ctx, "test_rack.pcm");

    rack.insert_effect(&eff_sine);
    rack.insert_effect(&eff_dist);
    rack.insert_effect(&eff_write);
    Wavr::Rack::data_t data(channels);
    for (size_t i = 0; i < data.size(); i++) {
        data[i].resize(frame_size);
    }
    for (size_t i = 0; i < total_samples; i += frame_size) {
        rack.process(data);
    }

    return 0;
}
