#include "context.h"
#include "effect.h"
#include <cmath>
#include <iostream>

#include "test_effects.h"

int main() {
    unsigned long samplerate = 44100L;
    unsigned short channels = 2;
    unsigned int frame_size = 256;
    Wavr::AudioContext ctx(samplerate, channels);
    Wavr::Test::SineWaveEffect sine(ctx, 60, 0.8);
    Wavr::Test::DistortionEffect dist(ctx, 2.0);
    Wavr::Test::PCMWriterEffect writer(ctx, "test_effect.pcm");
    unsigned long total_samples = samplerate * 10;

    ctx.set_state(Wavr::CONTEXT_STATE::STATE_PLAYING);
    Wavr::Effect::data_t data(channels);
    for (size_t i = 0; i < channels; i++) {
        data[i].resize(frame_size);
    }
    for (size_t i = 0; i < total_samples; i += frame_size) {
        sine.process(ctx, data);
        dist.process(ctx, data);
        writer.process(ctx, data);
    }

    return 0;
}
