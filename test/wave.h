
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>

// Wav Header
struct wav_header_t {
    char chunk_id[4];             //"RIFF" = 0x46464952
    unsigned long chunk_size;     // 28 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes] + sum(sizeof(chunk.id) +
                                  // sizeof(chunk.size) + chunk.size)
    char format[4];               //"WAVE" = 0x45564157
    char subckunh1_id[4];         //"fmt " = 0x20746D66
    unsigned long subchunk1_size; // 16 [+ sizeof(wExtraFormatBytes) + wExtraFormatBytes]
    unsigned short audio_format;
    unsigned short num_channels;
    unsigned long sample_rate;
    unsigned long byte_rate;
    unsigned short block_align;
    unsigned short bits_per_sample;
    //[WORD wExtraFormatBytes;]
    //[Extra format bytes]
};

// Chunks
struct chunk_t {
    char ID[4];        //"data" = 0x61746164
    unsigned int size; // Chunk data bytes
};

class WavStreamer {
public:
    WavStreamer(std::string filename, unsigned long sample_rate, unsigned short channels) {
        wave = fopen(filename.c_str(), "wb");
        strcpy(h.chunk_id, "RIFF");
        strcpy(h.format, "WAVE");
        strcpy(h.subckunh1_id, "fmt");
        h.chunk_size = 28;
        h.subchunk1_size = 16;
        h.audio_format = 1;
        h.num_channels = channels;
        h.sample_rate = sample_rate;
        h.block_align = sizeof(float) * 2 * channels + sizeof(chunk_t);
        h.byte_rate = sizeof(float) * sample_rate * channels;
        h.bits_per_sample = sizeof(float) * 8 * channels;
        fwrite(&h, sizeof(h), 1, wave);
    }
    ~WavStreamer() {
        if (wave != nullptr) {
            close();
        }
    }
    bool close() {
        fclose(wave);
        delete wave;
        return true;
    }
    bool write(unsigned long size, float *data) {
        unsigned long step = h.num_channels * 2 * sizeof(float);
        for (unsigned long i = 0; i < size * h.num_channels; i += step) {
            chunk_t ch;
            strcpy(ch.ID, "data");
            ch.size = step;
            fwrite(&ch, sizeof(ch), 1, wave);
            fwrite(data + i, sizeof(float), h.num_channels * 2, wave);
        }
        return true;
    }

private:
    FILE *wave;
    wav_header_t h;
};
