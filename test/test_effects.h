#pragma once
#include "effect.h"
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <unistd.h>

std::string get_filename(FILE *fd) {
    int fno = fileno(fd);
    char path[256];
    char result[256];
    sprintf(path, "/proc/self/fd/%i", fno);
    memset(result, 0, sizeof(result));
    readlink(path, result, sizeof(result) - 1);

    return std::string(result);
}

namespace Wavr {
namespace Test {
class SineWaveEffect : public Effect {
public:
    SineWaveEffect(AudioContext &context, float freq, float amplitude)
        : Effect(context), m_freq(freq), m_amp(amplitude), m_samples(0L) {}
    void set_freq(float f) { m_freq = f; }
    void set_amplitude(float f) { m_amp = f; }
    bool process(AudioContext &ctx, data_t &data) override {
        auto rate = ctx.get_samplerate();
        auto channels = ctx.get_channelcount();
        // This is a reasonable assumption that all channels will be of the same size
        // (something has gone horribly wrong if it doesn't)
        auto frame_size = data[0].size();
        for (unsigned int i = 0; i < frame_size; i++) {
            double t = (double)m_samples++ / rate;
            for (unsigned short c = 0; c < channels; c++) {
                data[c][i] = std::sin(M_PI * 2.0 * t * m_freq) * m_amp;
            }
        }

        return true;
    }

private:
    float m_freq;
    float m_amp;
    unsigned long m_samples;
};
class DistortionEffect : public Effect {
public:
    DistortionEffect(AudioContext &context, float force) : Effect(context), m_force(force) {}
    void set_force(double f) { m_force = f; }
    bool process(AudioContext &ctx, data_t &data) override {
        double step = 1.0 / ctx.get_samplerate();
        auto channels = ctx.get_channelcount();
        auto frame_size = data[0].size();
        auto amp_corr = std::min(std::abs(m_force), 1.0);

        for (unsigned int i = 0; i < frame_size; i++) {
            for (unsigned short c = 0; c < channels; c++) {
                data[c][i] = tanhf32(data[c][i] * m_force) / amp_corr;
            }
        }

        return true;
    }

private:
    double m_force;
};
class PCMWriterEffect : public Effect {
public:
    PCMWriterEffect(AudioContext &ctx, std::string filename) : Effect(ctx) { m_file = fopen(filename.c_str(), "wb"); }
    ~PCMWriterEffect() {
        if (m_file != nullptr)
            close();
    }
    void close() { fclose(m_file); }
    bool process(AudioContext &ctx, data_t &data) override {
        float *buffer = new float[data.size() * data[0].size()];
        for (size_t i = 0; i < data[0].size(); i++) {
            for (size_t c = 0; c < data.size(); c++) {
                buffer[i * data.size() + c] = data[c][i];
            }
        }
        fwrite(buffer, sizeof(float), ctx.get_channelcount() * data[0].size(), m_file);
#if DEBUG && VERBOSE
        std::cout << "Writing to file '" << get_filename(m_file) << "' "
                  << sizeof(float) * ctx.get_channelcount() * data[0].size() << "b" << std::endl;
#endif
        return false;
    }

private:
    FILE *m_file;
};
} // namespace Test
} // namespace Wavr
